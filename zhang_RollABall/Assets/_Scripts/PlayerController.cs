﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    //countdown timer
    float currentTime = 0f;
    float startingTime = 30f;

    //all the text display in game
    public float speed;
    public Text countText;
    public Text winText;
    public Text loseText;
    public Text timerText;
    public Text restartText;

    bool gameFinished;
    //In order to stop the game

    private Rigidbody rb;
    private int count;



    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText ();
        winText.text = "";
        loseText.text = "";
        restartText.text = "";
   
   
        currentTime = startingTime;    
    }

    // Update is called once per frame
    void Update()
    {

        //when the gane is not finished yet, continuing couting down the time
        if (gameFinished == false)
        {
            currentTime -= 1 * Time.deltaTime;
            timerText.text = "Time Left: " + currentTime.ToString("0");
        }
        
        //no negative number for timer
        if(currentTime < 0)
        {
            currentTime = 0;
         

            //game over situation
            if (count < 11)
            {
                loseText.text = "Game over :(";
                gameFinished = true;
                //appear when player loses game
                restartText.text = "Press space to restart";


              /*Invoke("RestartLevel", 3f); 
               * this is for automatically restart the scene
               */

            }
            
        }

        // press the space to restart the game
        if (gameFinished && Input.GetKeyDown (KeyCode.Space))
        {
            RestartLevel();
        }

    }

    private void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed);
    }

    private void OnTriggerEnter(Collider other)
    {
        //counting the quantity of pickups
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive (false);
            count = count + 1;
            SetCountText ();
        }
    }
    void SetCountText ()
    {
        countText.text = "Count: " + count.ToString ();

        //Game win situation
        if (count >= 11)
        {
            winText.text = "You Win!";
            gameFinished = true;
            //display when player wins the game
            restartText.text = "Press space to restart";

            /*Invoke("RestartLevel", 3f);
             * this is for automatically restart the scene
             */
        }
  
    }

    //restart the scene coding
    void RestartLevel ()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}